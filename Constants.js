export const SPLASH_IMAGE = require('./assets/images/parlour-splash.png');
export const ICON_IMAGE = require('./assets/images/parlour-logo.png');
export const LATO_REGULAR_ASSET = require('./assets/fonts/Slabo27px-Regular.ttf');
export const LATO_REGULAR_NAME = 'lato-regular';
export const KLAVIKA_BOLD_ASSET = require('./assets/fonts/klavika-bold.otf');
export const KLAVIKA_BOLD_NAME = 'klavika-bold';
export const LOGIN_BUTTON_TEXT = 'Login';
export const LOGIN_SUBMIT_TEXT = 'Entrar';
export const DROPDOWN_TEXT = 'Expandir';
export const PRIMARY_COLOR = '#E7397E';
export const PRIMARY_LIGHT_COLOR = '#e7397e6b';
export const SHADOW_COLOR = '#CCCCCC';
export const SPLASH_SCREEN_TIMING = 2000;
export const stepTexts = [
  'Todos os serviços na palma da sua mão',
  'Agilidade e conforto na sua casa',
  'As melhores profissionais do mercado'
]
