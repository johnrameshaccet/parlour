import React from 'react';
import { Dimensions, View, StyleSheet } from 'react-native';
import { LOGIN_BUTTON_TEXT, DROPDOWN_TEXT, PRIMARY_COLOR, LOGIN_SUBMIT_TEXT } from '../Constants';
import Button from './ui/Button';
import Logo from '../components/Logo';
import StepsCarousel from '../components/StepsCarousel';
import {Item, Picker } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
export const { width, height } = Dimensions.get('window');

export default class Steps extends React.Component {
	constructor(props) {
    super(props);
    this.state = {
      selected: undefined
    };
  }
	static navigationOptions = {
      header: null
  }
  onValueChange(value: string) {
    this.setState({
      selected: value
    });
  }
	render() {
		return (
			<View style={wrapperStyle}>
				<View style={wrapperStyle.logoContainer}>
					<Logo />
				</View>
				<View style={{ height: height * 0.3 }}>
					<StepsCarousel />
				</View>
              <Picker
                mode="dropdown"
                iosIcon={<Ionicons name="ios-arrow-down" size={32} color={PRIMARY_COLOR} />}
                style={{
									margin:20,
									width: width-50,
									borderRadius: 15,
					      	borderWidth: 3,
					      	borderColor: '#000000',
									padding:10
								}}
                placeholder={DROPDOWN_TEXT}
                placeholderStyle={{ color: "#000000"}}
                placeholderIconColor="#007aff"
                selectedValue={this.state.selected}
                onValueChange={this.onValueChange.bind(this)}
              >
                <Picker.Item label={DROPDOWN_TEXT} value="key0" />
              </Picker>

				<Button
					onPress={() => {}}
					text={LOGIN_BUTTON_TEXT}
					onPress={() => this.props.navigation.navigate('Login')}
				/>
			</View>
		);
	}
}

const wrapperStyle = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		alignContent: 'center'
	},
	logoContainer:{
		marginTop: 40,
		height: height * 0.22,
		width: width,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 50
	},
	child: {
		width: width
	}
});
