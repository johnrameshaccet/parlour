import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import { PRIMARY_COLOR, SHADOW_COLOR } from '../../Constants';

class Button extends Component {
	render() {
		const { text, onPress } = this.props;
		return (
			<TouchableOpacity style={styles.buttonStyle} onPress={() => onPress()}>
				<Text style={styles.textStyle}>{text}</Text>
			</TouchableOpacity>
		);
	}
}

Button.propTypes = {
	text: PropTypes.string.isRequired,
	onPress: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
	textStyle: {
		fontSize: 20,
		fontWeight:'bold',
		color: '#ffffff',
		textAlign: 'center'
	},

	buttonStyle: {
		padding: 10,
		margin: 10,
		backgroundColor: PRIMARY_COLOR,
		borderRadius: 15,
		shadowColor: SHADOW_COLOR,
		elevation: 2,
		shadowOffset: {
			width: 10,
			height: 10
		},
		height:50,
		shadowRadius: 5
	}
});

export default Button;
