import React, { PureComponent } from 'react';
import { Text, Dimensions, Image, StyleSheet, View } from 'react-native';
import { PRIMARY_COLOR, stepTexts, LATO_REGULAR_NAME } from '../Constants';
import SwiperFlatList from 'react-native-swiper-flatlist';

export const { width, height } = Dimensions.get('window');

export default class StepsCarousel extends PureComponent {
	render() {
		return (
			<View style={styles.container}>
				<SwiperFlatList
					autoplay
					autoplayDelay={2}
					autoplayLoop
					showPagination
					paginationActiveColor={PRIMARY_COLOR}
					paginationStyle={{ paddingTop: 50 }}
					paginationStyleItem={{ height: 10, width: 10 }}
				>
					{stepTexts.map((text, index) => (
						<View style={[styles.child]} key={index}>
							<Text style={styles.text}>{text}</Text>
						</View>
					))}
				</SwiperFlatList>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white'
	},
	child: {
		width,
		backgroundColor: 'white',
		justifyContent: 'center'
	},
	text: {
		color: '#000',
		fontWeight: 'bold',
		fontSize: 17,
		fontFamily: LATO_REGULAR_NAME,
		textAlign: 'center'
	}
});
