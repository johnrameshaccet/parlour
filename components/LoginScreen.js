import React, { Component } from 'react';
import {
	LOGIN_SUBMIT_TEXT,
	LATO_REGULAR_NAME,
	KLAVIKA_BOLD_NAME,
	PRIMARY_COLOR,
	PRIMARY_LIGHT_COLOR
} from '../Constants.js';
import { Dimensions, View, StyleSheet } from 'react-native';
import { Form, Item, Input, Text, Icon } from 'native-base';
import Button from './ui/Button';
import Logo from '../components/Logo';
export const { width, height } = Dimensions.get('window');

export default class LoginScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			username: '',
			password: ''
		};
	}

	static navigationOptions = {
		header: null
	};

	onLogin() {
		const { username, password } = this.state;

		Alert.alert('Credentials', `${username} + ${password}`);
	}

	render() {
		return (
			<View style={wrapperStyle}>
				<View style={wrapperStyle.logoContainer}>
					<Logo />
				</View>
				<Text style={wrapperStyle.headerText}>
					"Os botões desta tela são apenas ilustrativos para este teste"
				</Text>
				<View>
					<Form>
						<Item style={wrapperStyle.inputItem}>
							<Input
								style={wrapperStyle.input}
								placeholder="Usúarlo"
								value={this.state.uname}
								onChangeText={unameInput => {
									let lowerUnameInput = unameInput.toLowerCase();
									this.setState({ uname: lowerUnameInput });
								}}
							/>
						</Item>
						<Item style={wrapperStyle.inputItem}>
							<Input
								style={wrapperStyle.input}
								placeholder="Senha"
								secureTextEntry
								onChangeText={passwordInput =>
									this.setState({ password: passwordInput })
								}
							/>
						</Item>
						<Button
							onPress={() => {}}
							text={LOGIN_SUBMIT_TEXT}
							onPress={() => this.props.navigation.navigate('Steps')}
						/>
					</Form>
				</View>
				<Text style={{ textAlign: 'center', fontFamily: LATO_REGULAR_NAME }}>
					Não tem uma conta? Cadastrar
				</Text>
				<View style={wrapperStyle.loginFooter}>
					<Text style={wrapperStyle.ourTeamText}>
						our entre com
					</Text>
				</View>
				<Text style={wrapperStyle.socialIconSec}>
					f
				</Text>
			</View>
		);
	}
}
const wrapperStyle = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		alignContent: 'center'
	},
	child: {
		width: width
	},
	logoContainer:{
		marginTop: 40,
		height: height * 0.15,
		width: width,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 50
	},
	headerText:{
		paddingRight: 5,
		paddingLeft: 5,
		textAlign: 'center',
		fontSize: 18,
		marginTop: 40,
		fontFamily: LATO_REGULAR_NAME
	},
	input: {
		marginTop: 15,
		paddingLeft: 15,
		borderRadius: 15,
		borderWidth: 2,
		borderColor: '#000000',
		fontSize: 15,
		height: 40
	},
	inputItem: {
		width: width - 25,
		borderColor: "transparent"
	},
	loginFooter:{
		position: 'relative',
		borderBottomColor: PRIMARY_LIGHT_COLOR,
		borderBottomWidth: 2,
		justifyContent: 'center',
		alignContent: 'center',
		flex: 1,
		marginTop: 50
	},
	ourTeamText:{
		paddingLeft: 5,
		paddingRight: 5,
		marginLeft: width * 0.3,
		width: width * 0.4,
		fontSize: 12,
		position: 'absolute',
		backgroundColor: '#ffffff',
		textAlign: 'center'
	},
	socialIconSec:{
		top: height - 70,
		position: 'absolute',
		fontFamily: KLAVIKA_BOLD_NAME,
		fontSize: 50,
		fontWeight: 'bold',
		width: width,
		textAlign: 'center',
		color: PRIMARY_COLOR
	}
});
