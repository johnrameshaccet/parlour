import {
	SPLASH_IMAGE,
	ICON_IMAGE,
	LATO_REGULAR_ASSET,
	KLAVIKA_BOLD_ASSET,
	SPLASH_SCREEN_TIMING,
	PRIMARY_COLOR,
	SHADOW_COLOR
} from './Constants.js';
import React from 'react';
import { Image, Text, View, StyleSheet } from 'react-native';
import { Asset, AppLoading, Font, SplashScreen } from 'expo';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Steps from './components/Steps.js';
import LoginScreen from './components/LoginScreen';

const AppNavigator = createStackNavigator(
	{
		Steps: {
			screen: Steps
		},
		Login: {
			screen: LoginScreen
		}
	},
	{
		initialRouteName: 'Steps'
	}
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
	state = {
		isSplashReady: false,
		isAppReady: false
	};

	render() {
		if (!this.state.isSplashReady) {
			return (
				<AppLoading
					startAsync={this._cacheSplashResourcesAsync}
					onFinish={() => this.setState({ isSplashReady: true })}
					onError={console.warn}
					autoHideSplash={false}
				/>
			);
		}

		if (!this.state.isAppReady) {
			return (
				<View style={styles.container}>
				<View style={styles.splashImageItem}>
					<Image source={SPLASH_IMAGE} onLoad={this._cacheResourcesAsync} />
					</View>
				</View>
			);
		}

		return <AppContainer />;
	}

	_cacheSplashResourcesAsync = async () => {
		const gif = SPLASH_IMAGE;
		return Asset.fromModule(gif).downloadAsync();
	};

	_cacheResourcesAsync = async () => {
		SplashScreen.hide();
		const images = [ICON_IMAGE];

		const cacheImages = images.map(image => {
			return Asset.fromModule(image).downloadAsync();
		});
		await Font.loadAsync({
			'lato-regular': LATO_REGULAR_ASSET
		});

		await Font.loadAsync({
			'klavika-bold': KLAVIKA_BOLD_ASSET
		});
		await Promise.all(cacheImages);
		setTimeout(() => {
			this.setState({ isAppReady: true });
		}, SPLASH_SCREEN_TIMING);
	};
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		alignContent: 'center'
	},
	splashImageItem:{
		justifyContent: 'center',
		alignItems: 'center'
	},
	textStyle: {
		fontSize: 20,
		fontWeight:'bold',
		color: '#ffffff',
		textAlign: 'center'
	},
	buttonStyle: {
		padding: 10,
		margin: 10,
		backgroundColor: PRIMARY_COLOR,
		borderRadius: 15,
		shadowColor: SHADOW_COLOR,
		elevation: 2,
		shadowOffset: {
			width: 10,
			height: 10
		},
		height:50,
		shadowRadius: 5
	}
});
